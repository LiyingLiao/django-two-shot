from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView

from django.contrib.auth.mixins import LoginRequiredMixin

from receipts.models import Receipt, ExpenseCategory, Account

# Create your views here.
class ReceiptsListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list_receipts.html"

    def get_queryset(self):
        queryset = super().get_queryset().filter(purchaser=self.request.user)
        return queryset

    # or can use:
    # def get_queryset(self):
    #     return Receipt.objects.filter(purchaser=self.request.user)


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "receipts/create_receipt.html"
    fields = ["vendor", "total", "tax", "date", "category", "account"]
    success_url = reverse_lazy("home")

    def form_valid(self, form):
        form.instance.purchaser = self.request.user
        return super().form_valid(form)

    # the way showed in learn:
    # def form_valid(self, form):
    #     receipt = form.save(commit=False)
    #     receipt.purchaser = self.request.user
    #     receipt.save()
    #     return redirect("home")


class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "receipts/list_categories.html"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     user_receipts = self.request.user.receipts.all()

    # def group_receipts(receipts):
    #     result = {}
    #     for receipt in receipts:
    #         if receipt.category.name not in result:
    #             result[receipt.category.name] = 1
    #         else:
    #             result[receipt.category.name] += 1
    #     return result

    # grouped_receipts = group_receipts(user_receipts)
    # context["receipts"] = grouped_receipts
    # return context


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "receipts/list_accounts.html"

    def get_queryset(self):
        return super().get_queryset().filter(owner=self.request.user)

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     user_receipts = self.request.user.receipts.all()

    # def group_receipts(receipts):
    #     result = {}
    #     for receipt in receipts:
    #         if receipt.account.name not in result:
    #             result[receipt.account.name] = 1
    #         else:
    #             result[receipt.account.name] += 1
    #     return result

    # grouped_receipts = group_receipts(user_receipts)
    # context["receipts"] = grouped_receipts
    # return context


class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    fields = ["name"]
    template_name = "receipts/create_category.html"

    def form_valid(self, form):
        myform = form.save(commit=False)
        myform.owner = self.request.user
        myform.save()
        return redirect("list_categories")


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    fields = ["name", "number"]
    template_name = "receipts/create_account.html"
    success_url = reverse_lazy("list_accounts")

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)


# custom filter
from django.template.defaulttags import register


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)
