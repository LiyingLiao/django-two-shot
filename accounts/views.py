from django.shortcuts import redirect, render
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import login


# Create your views here.
def user_creation_view(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            # form.save()
            # user = User.objects.create_user(form)
            # user.save()
            user = form.save()
            login(request, user)
            return redirect("home")
        else:
            return redirect("signup")
    form = UserCreationForm()
    context = {"form": form}
    return render(request, "registration/signup.html", context)
